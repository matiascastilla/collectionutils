# encoding: utf-8

import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...

"Esto es un comentario"
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name="collectionutils",
    version="0.0.1",
    author="Matías Castilla",
    author_email="matiascastilla@gmail.com",
    description=("Helper functions for collections"),
    license="BSD",
    keywords="dictionaries collections",
    url="https://bitbucket.org/matiascastilla/collectionutils",
    packages=['collectionutils'],
    long_description=read('README'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
