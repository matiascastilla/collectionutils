def merge_dicts(dict1, dict2):
    return dict(dict1.items() + dict2.items())


KEYNOTFOUNDIN1 = '<KEYNOTFOUNDIN1>'  # KeyNotFound for dictDiff
KEYNOTFOUNDIN2 = '<KEYNOTFOUNDIN2>'  # KeyNotFound for dictDiff


def dict_diff(first, second):
    diff = {}
    sd1 = set(first)
    sd2 = set(second)
    for key in sd1.difference(sd2):
        diff[key] = KEYNOTFOUNDIN2
    for key in sd2.difference(sd1):
        diff[key] = KEYNOTFOUNDIN1
    for key in sd1.intersection(sd2):
        if first[key] != second[key]:
            diff[key] = (first[key], second[key])
    return diff


class Struct:
    def __init__(self, entries):
        self.__dict__.update(entries)


def omit(d, keys):
    """
    Elimina las claves y devuelve un nuevo diccionario
    """
    r = dict(d)
    for key in keys:
        if key in r:
            del r[key]
    return r


def replace(d, entries):
    new_dict = dict(d)
    for k, v in entries.iteritems():
        new_dict[k] = v
    return new_dict


def dict_copy(d):
    """
    Hace deep copy de los diccionarios sin copiar los objetos.
    """
    return {
        key: (dict_copy(value)
              if isinstance(value, dict) else
              [dict_copy(element) for element in value]
              if isinstance(value, list) else value
              for key, value in d.items())}


def map_keys(d, key_map):
    return dict((val, d[key]) for key, val in key_map.iteritems())


def dict_with_defaults(dict1, dict2):
    new = dict(dict1)
    for k, v in dict2.iteritems():
        if k not in new:
            new[k] = v
    return new
